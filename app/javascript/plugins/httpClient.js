const axios = require('axios')

const localHostClient = axios.create({
  baseURL: 'http://localhost:3000'
})

export default localHostClient;
