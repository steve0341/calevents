import Vue from 'vue'
import Router from 'vue-router'
import main from 'components/body/main'
import test from 'components/pages/test'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: "main", component: main},
    { path: '/foo', name: "Test", component: test}
  ]
})
