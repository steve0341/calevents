import { USER_REQUEST, USER_ERROR, USER_SUCCESS, USER_USERNAME_AVAILABLE, REGISTER } from '../actions/user'
import apiCall from 'plugins/api'
import Vue from 'vue'
import { AUTH_LOGOUT } from '../actions/auth'

const state = {
  status: '',
  profile: {},
  usernameAvailable: undefined
}

const getters = {
  getProfile: state => state.profile,
  isProfileLoaded: state => !!state.profile.name
}

const actions = {
  [USER_REQUEST]: ({commit, dispatch}, resp) => {
    commit(USER_REQUEST, resp.data)
    // apiCall({url: 'user/me'})
      // .then(resp => {
      //   commit(USER_SUCCESS, resp)
      // })
      // .catch(resp => {
      //   commit(USER_ERROR)
      //   // if resp is unauthorized, logout, to
      //   dispatch(AUTH_LOGOUT)
      // })
  },
  [USER_USERNAME_AVAILABLE]: ({commit, dispatch}, payload) => {
    return new Promise((resolve, reject) => {
      axios({ url: '/checkemail', data: payload, method: 'POST' })
      .then(resp => {
        resolve(resp.data);
      })
      .catch(err => {
        reject(err);
      })
    });
  },
  [REGISTER]: ({commit, dispatch}, payload) => {
    // payload.authenticity_token = document.getElementsByTagName("meta")[1].content
    return new Promise((resolve, reject) => {
      axios({ url: '/users', data: payload, method: 'POST' })
      .then(resp => {
        resolve(resp.data);
      })
      .catch(err => {
        reject(err);
      })
    });
  }
}

const mutations = {
  [USER_REQUEST]: (state, data) => {
    state.status = 'loading'
  },
  [USER_SUCCESS]: (state, resp) => {
    state.status = 'success'
    Vue.set(state, 'profile', resp)
  },
  [USER_ERROR]: (state) => {
    state.status = 'error'
  },
  [AUTH_LOGOUT]: (state) => {
    state.profile = {}
  },
  [USER_USERNAME_AVAILABLE]: (state, data) => {
    state.usernameAvailable = data.available;
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
