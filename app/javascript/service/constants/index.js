const LOGIN = '/users/sign_in';
const SIGN_OUT = '/users/sign_out';


module.exports = {
  LOGIN_URL,
  SIGN_OUT
}
