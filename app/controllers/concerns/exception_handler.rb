module ExceptionHandler
  # provides the more graceful `included` method
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordNotFound do |e|
      json_response({ message: e.message }, :not_found)
    end

    rescue_from ActiveRecord::RecordInvalid do |e|
      json_response({ message: e.message }, :unprocessable_entity)
    end

    rescue_from ActionController::InvalidAuthenticityToken do |e|
      json_response({ message: "User Token failed authentication" }, :unauthorized)
    end

    rescue_from ActiveRecord::RecordNotUnique do |e|
      json_response({ message: "A record already exists for the given attributes" }, :unprocessable_entity)
    end

    rescue_from ActionController::ParameterMissing do |e|
      json_response({ message: "#{e}" }, :unprocessable_entity)
    end

    rescue_from ActiveRecord::InvalidForeignKey do |e|
      json_response({ message: "#{e}" }, :unprocessable_entity)
    end
  end
end
