Rails.application.routes.draw do
  devise_for :users, controllers: { :registrations => 'users/registrations' }
  # resources :users
  post 'checkemail', to: 'users#check_email'
  get 'landing/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'landing#index'
end
